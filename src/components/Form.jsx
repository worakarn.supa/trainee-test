// eslint-disable-next-line no-unused-vars
import React, { useState } from "react";
import "./components.css";
import { useForm } from "react-hook-form";

export default function Form() {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm();

  const [formData, setFormData] = useState(null);

  const onSubmit = (data) => {
    setFormData(data);
    console.log(data);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label>
          Name <span className="redText">*</span>
          <input
            className={`normalField ${
              errors.name?.type === "required" ? "errorField" : ""
            }`}
            placeholder="Name"
            {...register("name", { required: true })}
          />
          {errors.name?.type === "required" && (
            <p className="errorText">This field is required</p>
          )}
        </label>
        <label>
          Email <span className="redText">*</span>
          <input
            className={`normalField ${
              errors.email?.type === "required" ? "errorField" : ""
            }`}
            placeholder="Email"
            {...register("email", {
              required: true,
              pattern: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/i,
            })}
          />
          {errors.email?.type === "required" && (
            <p className="errorText">Email is required</p>
          )}
          {errors.email?.type === "pattern" && (
            <p className="errorText">Entered email is in wrong format</p>
          )}
        </label>
        <label>
          Password <span className="redText">*</span>
          <input
            className={`normalField ${
              errors.password?.type === "required" ? "errorField" : ""
            }`}
            type="password"
            placeholder="Password"
            {...register("password", {
              required: true,
              minLength: 8,
            })}
          />
          {errors.password?.type === "minLength" && (
            <p className="errorText">
              Entered password is less than 8 characters
            </p>
          )}
          {errors.password?.type === "required" && (
            <p className="errorText">
              Entered password is less than 8 characters
            </p>
          )}
        </label>
        <label>
          Tel <span className="redText">*</span>
          <input
            className={`normalField ${
              errors.tel?.type === "required" ? "errorField" : ""
            }`}
            type="number"
            placeholder="Tel"
            onWheel={(e) => e.target.blur()}
            {...register("tel", {
              required: true,
              minLength: 10,
              maxLength: 10,
            })}
          />
          {errors.tel?.type === "required" && (
            <p className="errorText">tel is required field</p>
          )}
          {errors.tel?.type === "minLength" && (
            <p className="errorText">Entered number equal to 10 digits</p>
          )}
          {errors.tel?.type === "maxLength" && (
            <p className="errorText">Entered number equal to 10 digits</p>
          )}
        </label>
        <label>
          Address <span className="redText">*</span>
          <textarea
            className={`areaField ${
              errors.address?.type === "required" ? "errorAreaField" : ""
            }`}
            type="text"
            placeholder="Address"
            {...register("address", { required: true })}
          />
          {errors.address?.type === "required" && (
            <p className="errorText">address is required</p>
          )}
        </label>
        <label>
          Gender <span className="redText">*</span>
          <select
            className={`normalField ${
              errors.gender?.type === "required" ? "errorField" : ""
            }`}
            defaultValue=""
            {...register("gender", { required: true })}
          >
            <option disabled={true} value="">
              Select gender
            </option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
          {errors.gender && <p className="errorText">This field is required</p>}
        </label>
        <button className="summit-btn" type="submit">
          Submit
        </button>
      </form>
      <div className="showData">
        <p>Name : {formData?.name}</p>
        <p>Email : {formData?.email}</p>
        <p>Tel : {formData?.tel}</p>
        <p>Address : {formData?.address}</p>
        <p>Gender : {formData?.gender}</p>
      </div>
    </>
  );
}
